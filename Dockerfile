FROM node:lts-hydrogen
COPY package.json yarn.lock ./
RUN yarn install --frozen-lockfile

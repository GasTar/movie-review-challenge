import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'

import { AppController } from './app.controller'
import { AppService } from './app.service'

import configuration, { validationSchema } from './config/configuration'
import { AppConfig, Configuration } from './config/config.keys'
import { DatabaseModule } from './config/database/database.module'

import { UsersModule } from './users/users.module'
import { MoviesModule } from './movies/movies.module'
import { ReviewsModule } from './reviews/reviews.module'
import { ApiModule } from './api/api.module'

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [configuration],
      validationSchema,
    }),
    DatabaseModule,
    UsersModule,
    MoviesModule,
    ReviewsModule,
    ApiModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  static port: number
  static appName: string
  static appDescription: string
  static appVersion: string
  static swaggerPath: string

  constructor(private readonly _configService: ConfigService) {
    AppModule.port = this._configService.get<AppConfig>(Configuration.APP).port
    AppModule.appName = this._configService.get<AppConfig>(
      Configuration.APP,
    ).appName
    AppModule.appDescription = this._configService.get<AppConfig>(
      Configuration.APP,
    ).appDescription
    AppModule.appVersion = this._configService.get<AppConfig>(
      Configuration.APP,
    ).appVersion
    AppModule.swaggerPath = this._configService.get<AppConfig>(
      Configuration.APP,
    ).swaggerPath
  }
}

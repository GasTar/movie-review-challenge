import { LoggerOptions } from 'typeorm'

export interface AppConfig {
  port: number
  appName: string
  appDescription: string
  appVersion: string
  swaggerPath: string
}

export interface DbConfig {
  host: string
  type: string
  port: number
  username: string
  password: string
  database: string
  synchronize: boolean
  logging: LoggerOptions
}

export interface TheMovieDatabaseApiConfig {
  url: string
  token: string
}

export enum Configuration {
  APP = 'application',
  DB = 'database',
  THE_MOVIE_DATABASE_API = 'theMovieDatabaseApi',
}

import { DynamicModule } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PostgresConnectionOptions } from 'typeorm/driver/postgres/PostgresConnectionOptions'

import { Configuration, DbConfig } from '../config.keys'

export const databaseProviders: DynamicModule[] = [
  TypeOrmModule.forRootAsync({
    imports: [ConfigModule],
    inject: [ConfigService],
    useFactory: (config: ConfigService) => {
      const db_config = config.get<DbConfig>(Configuration.DB)

      return {
        host: db_config.host,
        type: db_config.type,
        port: db_config.port,
        username: db_config.username,
        password: db_config.password,
        database: db_config.database,
        synchronize: db_config.synchronize,
        logging: db_config.logging,
        autoLoadEntities: true,
        migrations: [__dirname + '/migrations/*.ts'],
        subscribers: [__dirname + '/subscribers/*.ts'],
      } as PostgresConnectionOptions
    },
  }),
]

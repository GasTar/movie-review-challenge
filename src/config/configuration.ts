import * as Joi from 'joi'
import { LoggerOptions } from 'typeorm'

const db_synchronize = (): boolean => process.env.DB_SYNCHRONIZE === 'true'

const db_logging = (): LoggerOptions =>
  process.env.NODE_ENV === 'production' ? false : 'all'

export const validationSchema = Joi.object({
  NODE_ENV: Joi.string()
    .valid('development', 'production')
    .default('development'),

  APP_PORT: Joi.number().integer().default(3000),
  APP_NAME: Joi.string().required(),
  APP_DESCRIPTION: Joi.string().required(),
  APP_VERSION: Joi.string().default('1.0.0'),

  SWAGGER_PATH: Joi.string().allow('').default('docs'),

  DB_TYPE: Joi.string().valid('postgres').default('postgres'),
  DB_PORT: Joi.number().integer().required(),
  DB_HOST: Joi.string().required(),
  DB_USER: Joi.string().required(),
  DB_PASSWORD: Joi.string().required(),
  DB_DATABASE: Joi.string().required(),
  DB_SYNCHRONIZE: Joi.boolean().default(false),
  DB_LOGGING: Joi.boolean().default(false),

  THE_MOVIE_DATABASE_API_URL: Joi.string().default(
    'https://api.themoviedb.org/3',
  ),
  THE_MOVIE_DATABASE_API_TOKEN: Joi.string().required(),
})

export default () => ({
  application: {
    port: parseInt(process.env.APP_PORT, 10) || 3000,
    appName: process.env.APP_NAME,
    appDescription: process.env.APP_DESCRIPTION,
    appVersion: process.env.APP_VERSION,
    swaggerPath: process.env.SWAGGER_PATH,
  },
  database: {
    host: process.env.DB_HOST,
    type: process.env.DB_TYPE,
    port: parseInt(process.env.DB_PORT, 10) || 5432,
    username: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database: process.env.DB_DATABASE,
    synchronize: db_synchronize(),
    logging: db_logging(),
  },
  theMovieDatabaseApi: {
    url: process.env.THE_MOVIE_DATABASE_API_URL,
    token: process.env.THE_MOVIE_DATABASE_API_TOKEN,
  },
})

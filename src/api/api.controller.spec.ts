import { Test, TestingModule } from '@nestjs/testing'
import { ApiController } from './api.controller'
import { ApiService } from './api.service'
import { ConfigService } from '@nestjs/config'
import { HttpService } from '@nestjs/axios'

describe('ApiController', () => {
  let controller: ApiController
  const mockConfigService = {}
  const mockHttpService = {}

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      controllers: [ApiController],
      providers: [ApiService, ConfigService, HttpService],
    })
      .overrideProvider(ConfigService)
      .useValue(mockConfigService)
      .overrideProvider(HttpService)
      .useValue(mockHttpService)
      .compile()

    controller = app.get<ApiController>(ApiController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })
})

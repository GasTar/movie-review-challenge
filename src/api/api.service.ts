import { HttpService } from '@nestjs/axios'
import {
  BadRequestException,
  Injectable,
  NotFoundException,
  ServiceUnavailableException,
  UnauthorizedException,
} from '@nestjs/common'
import { ConfigService } from '@nestjs/config'

import { lastValueFrom } from 'rxjs'
import { Configuration, TheMovieDatabaseApiConfig } from '../config/config.keys'

const axiosErrorsMessages = {
  unAuthorized: 'Request failed with status code 401',
  notFound: 'Request failed with status code 404',
}

// TODO: see where to put this interface
export interface IMovieDb {
  id: number
  overview: string
  poster_path: string
  release_date: string
  title: string
}

@Injectable()
export class ApiService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  // TODO: maybe as property insted of method
  private _getTheMovieDbConfig() {
    const theMovieDbConfigApi =
      this.configService.get<TheMovieDatabaseApiConfig>(
        Configuration.THE_MOVIE_DATABASE_API,
      ).url
    const TOKEN = this.configService.get<TheMovieDatabaseApiConfig>(
      Configuration.THE_MOVIE_DATABASE_API,
    ).token
    const options = {
      method: 'GET',
      headers: {
        accept: 'application/json',
        Authorization: `Bearer ${TOKEN}`,
      },
    }
    return { theMovieDbConfigApi, options }
  }

  async getTheMovieDb(tmdbId: number) {
    if (!tmdbId) throw new BadRequestException('tmdbId is required')

    const { theMovieDbConfigApi, options } = this._getTheMovieDbConfig()
    const url = `${theMovieDbConfigApi}/movie/${tmdbId}`

    try {
      const response = await lastValueFrom(this.httpService.get(url, options))
      const movie: IMovieDb = response.data

      return movie
    } catch (error) {
      if (error.message === axiosErrorsMessages.unAuthorized)
        throw new UnauthorizedException('The Movie Database API: invalid Token')

      if (error.message === axiosErrorsMessages.notFound)
        throw new NotFoundException('The Movie Database API: movie not found')

      throw new ServiceUnavailableException(
        'The Movie Database API is not avaiable: maybe wrong url',
      )
    }
  }
}

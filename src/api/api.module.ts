import { Module } from '@nestjs/common'
import { HttpModule } from '@nestjs/axios'

import { ApiController } from './api.controller'
import { ApiService } from './api.service'
import { ConfigModule, ConfigService } from '@nestjs/config'

@Module({
  imports: [HttpModule, ConfigModule],
  controllers: [ApiController],
  providers: [ApiService, ConfigService],
  exports: [HttpModule, ConfigModule],
})
export class ApiModule {}

import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import { ValidationPipe } from '@nestjs/common'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'

async function bootstrap() {
  const app = await NestFactory.create(AppModule)

  app.useGlobalPipes(
    new ValidationPipe({
      transform: true,
      whitelist: true,
    }),
  )

  const config = new DocumentBuilder()
    .setTitle(AppModule.appName)
    .setDescription(AppModule.appDescription)
    .setVersion(AppModule.appVersion)
    .build()
  const document = SwaggerModule.createDocument(app, config)
  SwaggerModule.setup(AppModule.swaggerPath, app, document)

  console.log('Running on port: ', AppModule.port)
  await app.listen(AppModule.port)
}
bootstrap()

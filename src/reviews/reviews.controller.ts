import { BadRequestException, Body, Controller, Post } from '@nestjs/common'

import { ReviewsService } from './reviews.service'
import { CreateReviewDto } from './dto/create-review.dto'
import { UsersService } from '../users/users.service'
import { MoviesService } from '../movies/movies.service'
import {
  ApiBadRequestResponse,
  ApiCreatedResponse,
  ApiInternalServerErrorResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger'

@Controller('reviews')
export class ReviewsController {
  constructor(
    private readonly reviewsService: ReviewsService,
    private readonly usersService: UsersService,
    private readonly moviesService: MoviesService,
  ) {}

  @ApiCreatedResponse({
    description:
      "Created review, created movie data if doesn't exist, created user if doesn't exist",
  })
  @ApiBadRequestResponse({ description: 'Bad request' })
  @ApiUnauthorizedResponse({ description: 'Unauthorized' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Post()
  async create(@Body() review: CreateReviewDto) {
    const exists = await this.reviewsService.exists(review)
    if (exists) throw new BadRequestException('review already exists')

    const user = await this.usersService.findOrCreate({
      userName: review.userName,
    })
    const movie = await this.moviesService.findOrCreate(review.tmdbId)

    return await this.reviewsService.create({ ...review, user, movie })
  }
}

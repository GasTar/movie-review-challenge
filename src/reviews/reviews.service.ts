import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

import { IReview, Review } from './review.entity'
import { CreateReviewDto } from './dto/create-review.dto'

@Injectable()
export class ReviewsService {
  constructor(
    @InjectRepository(Review)
    private reviewsRepository: Repository<Review>,
  ) {}

  exists(review: CreateReviewDto) {
    return this.reviewsRepository.exist({
      where: {
        movie: {
          tmdbId: review.tmdbId,
        },
        user: {
          userName: review.userName,
        },
      },
    })
  }

  findByUserUserName(userName: string) {
    return this.reviewsRepository.find({
      relations: ['movie'],
      where: {
        user: {
          userName,
        },
      },
    })
  }

  findByTmdbId(tmdbId: number) {
    return this.reviewsRepository.find({
      relations: ['movie'],
      where: {
        movie: {
          tmdbId,
        },
      },
    })
  }

  async create(review: IReview) {
    return await this.reviewsRepository.save(review)
  }
}

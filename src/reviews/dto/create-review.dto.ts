import { ApiProperty } from '@nestjs/swagger'
import { IsInt, IsNotEmpty, IsString, Max, Min } from 'class-validator'

export class CreateReviewDto {
  @ApiProperty({ example: 100 })
  @IsInt()
  @IsNotEmpty()
  tmdbId: number

  @ApiProperty({ example: 'John Doe' })
  @IsString()
  @IsNotEmpty()
  userName: string

  @ApiProperty({ example: 8 })
  @IsInt()
  @Min(1)
  @Max(10)
  @IsNotEmpty()
  rating: number
}

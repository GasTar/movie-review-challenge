import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { User } from '../users/user.entity'
import { Movie } from '../movies/movie.entity'

@Entity({ name: 'review' })
export class Review {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ type: 'int' })
  rating: number

  // relations

  @ManyToOne(() => User, (user) => user.reviews)
  user: User

  @ManyToOne(() => Movie, (movie) => movie.reviews)
  movie: Movie
}

export interface IReview {
  rating: number
  user: User
  movie: Movie
}

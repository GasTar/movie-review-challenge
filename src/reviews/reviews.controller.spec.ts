import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'

import { MockRepository } from '../utils/testing/MockRepository'
import { ReviewsService } from '../reviews/reviews.service'
import { ReviewsController } from './reviews.controller'
import { UsersService } from '../users/users.service'
import { MoviesService } from '../movies/movies.service'
import { Review } from './review.entity'
import { CreateReviewDto } from './dto/create-review.dto'
import { BadRequestException } from '@nestjs/common'

describe('ReviewsController', () => {
  let controller: ReviewsController

  const mockReviewsService = {
    exists: jest.fn(),
    create: jest.fn(() => ({})),
  }
  const mockUsersService = {
    findOrCreate: jest.fn(),
  }
  const mockMoviesService = {
    create: jest.fn(() => ({})),
    findOrCreate: jest.fn(),
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ReviewsController],
      providers: [
        ReviewsService,
        { provide: getRepositoryToken(Review), useClass: MockRepository },
        UsersService,
        MoviesService,
      ],
    })
      .overrideProvider(UsersService)
      .useValue(mockUsersService)
      .overrideProvider(ReviewsService)
      .useValue(mockReviewsService)
      .overrideProvider(MoviesService)
      .useValue(mockMoviesService)
      .compile()

    controller = module.get<ReviewsController>(ReviewsController)
  })

  afterEach(() => {
    jest.restoreAllMocks()
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  it('should create a review', () => {
    const review: CreateReviewDto = {
      tmdbId: 100,
      rating: 6,
      userName: 'gasti',
    }
    const existsMock = jest
      .spyOn(mockReviewsService, 'exists')
      .mockImplementation(() => false)

    expect(controller.create(review)).toStrictEqual(Promise.resolve({}))
    expect(existsMock).toHaveBeenCalledTimes(1)
  })

  it('should throw a BadRequestResponse error when review exists', async () => {
    const review: CreateReviewDto = {
      tmdbId: 100,
      rating: 6,
      userName: 'gasti',
    }
    const notExistMock = jest
      .spyOn(mockReviewsService, 'exists')
      .mockImplementation(() => true)

    try {
      await controller.create(review)
    } catch (error) {
      expect(error).toBeInstanceOf(BadRequestException)
    }

    expect(notExistMock).toHaveBeenCalledTimes(1)
  })
})

import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'
import { ReviewsService } from './reviews.service'
import { Review } from './review.entity'

describe('ReviewsService', () => {
  let service: ReviewsService
  const mockReviewRepository = {
    findOne: jest.fn((dto) => dto),
    find: jest.fn().mockReturnValue([]),
    save: jest.fn((dto) => dto),
    exist: jest.fn(() => true),
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ReviewsService,
        { provide: getRepositoryToken(Review), useValue: mockReviewRepository },
      ],
    }).compile()

    service = module.get<ReviewsService>(ReviewsService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  it('should create a review', async () => {
    const review = {
      rating: 8,
      user: {
        id: '',
        userName: '',
        reviews: [],
      },
      movie: {
        id: '',
        tmdbId: 100,
        title: '',
        release_date: new Date(),
        poster: '',
        overview: '',
        reviews: [],
      },
    }

    expect(await service.create(review)).toStrictEqual(review)
  })

  it('should find reviews by tmdbId', () => {
    const tmdbId = 100

    expect(service.findByTmdbId(tmdbId)).toStrictEqual([])
  })

  it('should find reviews by userName', () => {
    const userName = 'John Doe'

    expect(service.findByUserUserName(userName)).toStrictEqual([])
  })

  it('should return boolean if exists', () => {
    const review = {
      tmdbId: 100,
      userName: 'John Doe',
      rating: 3,
    }
    expect(service.exists(review)).toStrictEqual(true)
  })
})

import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'
import { MoviesService } from './movies.service'
import { Movie } from './movie.entity'
import { ApiService } from '../api/api.service'

describe('MoviesService', () => {
  let service: MoviesService
  const mockMovieRepository = {
    findOne: jest.fn((dto) => dto),
    save: jest.fn((dto) => dto),
  }
  const mockApiService = {
    getTheMovieDb: jest.fn((tmdbId) => tmdbId),
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        MoviesService,
        { provide: getRepositoryToken(Movie), useValue: mockMovieRepository },
        ApiService,
      ],
    })
      .overrideProvider(ApiService)
      .useValue(mockApiService)
      .compile()

    service = module.get<MoviesService>(MoviesService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  it('should format movie Data', () => {
    const movieDbData = {
      id: 100,
      tmdbId: 100,
      overview: '',
      poster_path: '',
      release_date: '2023-08-08',
      title: '',
    }

    expect(service._formatMovieDb(movieDbData)).toStrictEqual({
      tmdbId: movieDbData.id,
      title: movieDbData.title,
      release_date: new Date(movieDbData.release_date),
      poster: movieDbData.poster_path,
      overview: movieDbData.overview,
    })
  })

  it('should create a movie', async () => {
    const movie = {
      tmdbId: 100,
      title: 'titleee',
      poster: 'posterrr',
      overview: 'overvieww',
      release_date: new Date(),
    }

    expect(await service.create(movie)).toStrictEqual(movie)
  })

  it('should find a movie by its tmdbId', async () => {
    const tmdbId = 100

    expect(await service.findByTmdbId(tmdbId)).toStrictEqual({
      where: {
        tmdbId,
      },
    })
  })

  it("should find and create a movie if it doesn't exists", async () => {
    const tmdbId = 100
    const mockMovieData = {
      id: '',
      tmdbId: 100,
      overview: '',
      poster: '',
      release_date: new Date(),
      title: '',
    }
    const notExistsMock = jest
      .spyOn(service, 'findByTmdbId')
      .mockImplementation(() => null)
    const createSpy = jest.spyOn(service, 'create')
    const formatMovieDbSpy = jest
      .spyOn(service, '_formatMovieDb')
      .mockImplementation(() => mockMovieData)

    expect(await service.findOrCreate(tmdbId)).toStrictEqual(mockMovieData)
    expect(notExistsMock).toBeCalledTimes(1)
    expect(createSpy).toBeCalledTimes(1)
    expect(formatMovieDbSpy).toBeCalledTimes(1)
  })

  it("should find and create an user if it doesn't exists", async () => {
    const tmdbId = 100
    const mockMovieData = {
      id: '',
      tmdbId: 100,
      overview: '',
      poster: '',
      release_date: new Date(),
      title: '',
      reviews: [],
    }
    const notExistsMock = jest
      .spyOn(service, 'findByTmdbId')
      .mockReturnValue(Promise.resolve(mockMovieData))
    const createSpy = jest.spyOn(service, 'create')
    const formatMovieDbSpy = jest
      .spyOn(service, '_formatMovieDb')
      .mockImplementation(() => mockMovieData)

    expect(await service.findOrCreate(tmdbId)).toStrictEqual(mockMovieData)
    expect(notExistsMock).toBeCalledTimes(1)
    expect(createSpy).toBeCalledTimes(0)
    expect(formatMovieDbSpy).toBeCalledTimes(0)
  })
})

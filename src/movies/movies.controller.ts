import { Controller, Get, Param, ParseIntPipe } from '@nestjs/common'

import { MoviesService } from './movies.service'
import { ReviewsService } from '../reviews/reviews.service'
import { ApiInternalServerErrorResponse, ApiOkResponse } from '@nestjs/swagger'

@Controller('movies')
export class MoviesController {
  constructor(
    private readonly moviesService: MoviesService,
    private readonly reviewsService: ReviewsService,
  ) {}

  @ApiOkResponse({
    description: 'List reviews for movie by tmdbId',
    isArray: true,
  })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Get(':tmdbId/reviews')
  findReviewsById(@Param('tmdbId', ParseIntPipe) tmdbId: number) {
    return this.reviewsService.findByTmdbId(tmdbId)
  }
}

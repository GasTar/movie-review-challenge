import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'
import { Review } from '../reviews/review.entity'

@Entity({ name: 'movie' })
export class Movie {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ type: 'int', unique: true })
  tmdbId: number

  @Column()
  title: string

  @Column({ type: 'timestamptz' })
  release_date: Date

  @Column()
  poster: string

  @Column()
  overview: string

  // relations

  @OneToMany(() => Review, (review) => review.movie)
  reviews: Review[]
}

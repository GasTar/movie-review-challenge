import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

import { Movie } from './movie.entity'
import { CreateMovieDto } from './dto/create-movie.dto'
import { ApiService, IMovieDb } from '../api/api.service'

@Injectable()
export class MoviesService {
  constructor(
    @InjectRepository(Movie)
    private moviesRepository: Repository<Movie>,
    private readonly apiService: ApiService,
  ) {}

  // TODO: maybe some design pattern
  _formatMovieDb(movieDbData: IMovieDb) {
    return {
      tmdbId: movieDbData.id,
      title: movieDbData.title,
      release_date: new Date(movieDbData.release_date),
      poster: movieDbData.poster_path,
      overview: movieDbData.overview,
    }
  }

  async findByTmdbId(tmdbId: number) {
    return await this.moviesRepository.findOne({ where: { tmdbId } })
  }

  async create(movie: CreateMovieDto) {
    return await this.moviesRepository.save(movie)
  }

  async findOrCreate(tmdbId: number) {
    const movie = await this.findByTmdbId(tmdbId)
    if (!movie) {
      const movieData: IMovieDb = await this.apiService.getTheMovieDb(tmdbId)

      return await this.create(this._formatMovieDb(movieData))
    }

    return movie
  }
}

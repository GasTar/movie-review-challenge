import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'

import { MockRepository } from '../utils/testing/MockRepository'
import { ReviewsService } from '../reviews/reviews.service'
import { MoviesService } from '../movies/movies.service'
import { MoviesController } from './movies.controller'
import { Movie } from './movie.entity'

describe('MoviesController', () => {
  let controller: MoviesController

  const mockReviewsService = {
    findByTmdbId: jest.fn((tmdbId) => ({ movie: { tmdbId } })),
  }
  const mockMoviesService = {}

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MoviesController],
      providers: [
        ReviewsService,
        { provide: getRepositoryToken(Movie), useClass: MockRepository },
        MoviesService,
      ],
    })
      .overrideProvider(MoviesService)
      .useValue(mockMoviesService)
      .overrideProvider(ReviewsService)
      .useValue(mockReviewsService)
      .compile()

    controller = module.get<MoviesController>(MoviesController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  it('should return reviews for a particular movie', () => {
    const tmdbId = 100
    expect(controller.findReviewsById(tmdbId)).toStrictEqual({
      movie: { tmdbId },
    })
    expect(mockReviewsService.findByTmdbId).toBeCalledTimes(1)
  })
})

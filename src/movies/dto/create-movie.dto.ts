import { IsDateString, IsNotEmpty, IsNumber, IsString } from 'class-validator'

export class CreateMovieDto {
  @IsNumber()
  @IsNotEmpty()
  tmdbId: number

  @IsString()
  @IsNotEmpty()
  title: string

  @IsString()
  @IsNotEmpty()
  poster: string

  @IsString()
  @IsNotEmpty()
  overview: string

  // TODO: check IsDate() how works
  @IsDateString()
  @IsNotEmpty()
  release_date: Date
}

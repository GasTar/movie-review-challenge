import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'

import { MoviesService } from './movies.service'
import { MoviesController } from './movies.controller'
import { Movie } from './movie.entity'
import { Review } from '../reviews/review.entity'
import { ReviewsService } from '../reviews/reviews.service'
import { ApiModule } from '../api/api.module'
import { ApiService } from '../api/api.service'

@Module({
  imports: [TypeOrmModule.forFeature([Movie, Review]), ApiModule],
  controllers: [MoviesController],
  providers: [MoviesService, ReviewsService, ApiService],
  exports: [MoviesService],
})
export class MoviesModule {}

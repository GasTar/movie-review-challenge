import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm'

import { Review } from '../reviews/review.entity'

@Entity({ name: 'user' })
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string

  @Column({ unique: true })
  userName: string

  // relations

  @OneToMany(() => Review, (review) => review.user)
  reviews: Review[]
}

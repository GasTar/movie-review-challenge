import { Controller, Get, Inject, Param, forwardRef } from '@nestjs/common'

import { UsersService } from './users.service'
import { ReviewsService } from '../reviews/reviews.service'
import {
  ApiOkResponse,
  ApiInternalServerErrorResponse,
  ApiServiceUnavailableResponse,
} from '@nestjs/swagger'

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    @Inject(forwardRef(() => ReviewsService))
    private readonly reviewsService: ReviewsService,
  ) {}

  @ApiOkResponse({
    description: 'List reviews for user by userName',
    isArray: true,
  })
  @ApiServiceUnavailableResponse({ description: 'Service Unavailable' })
  @ApiInternalServerErrorResponse({ description: 'Internal server error' })
  @Get(':userName/reviews')
  findReviewsByUserName(@Param('userName') userName: string) {
    return this.reviewsService.findByUserUserName(userName)
  }
}

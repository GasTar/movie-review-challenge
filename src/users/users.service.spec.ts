import { Test, TestingModule } from '@nestjs/testing'
import { getRepositoryToken } from '@nestjs/typeorm'
import { User } from './user.entity'
import { UsersService } from './users.service'

describe('UsersService', () => {
  let service: UsersService
  const mockUserRepository = {
    findOne: jest.fn((dto) => dto),
    save: jest.fn((dto) => dto),
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        UsersService,
        { provide: getRepositoryToken(User), useValue: mockUserRepository },
      ],
    }).compile()

    service = module.get<UsersService>(UsersService)
  })

  it('should be defined', () => {
    expect(service).toBeDefined()
  })

  it('should create an user', async () => {
    const user = {
      userName: 'John Doe',
    }

    expect(await service.create(user)).toStrictEqual(user)
  })

  it('should find an user by its userName', async () => {
    const userName = 'John Doe'

    expect(await service.findByUserName(userName)).toStrictEqual({
      where: {
        userName,
      },
    })
  })

  it("should find and create an user if it doesn't exists", async () => {
    const userName = 'John Doe'
    const notExistsMock = jest
      .spyOn(service, 'findByUserName')
      .mockImplementation(() => null)
    const createSpy = jest.spyOn(service, 'create')

    expect(await service.findOrCreate({ userName })).toStrictEqual({
      userName,
    })
    expect(notExistsMock).toBeCalledTimes(1)
    expect(createSpy).toBeCalledTimes(1)
  })

  it("should find and create an user if it doesn't exists", async () => {
    const userName = 'John Doe'
    const mockUser = {
      id: 'gasti',
      userName: 'gasti',
      reviews: [],
    }
    const notExistsMock = jest
      .spyOn(service, 'findByUserName')
      .mockReturnValue(Promise.resolve(mockUser))
    const createSpy = jest.spyOn(service, 'create')

    expect(await service.findOrCreate({ userName })).toStrictEqual(mockUser)
    expect(notExistsMock).toBeCalledTimes(1)
    expect(createSpy).toBeCalledTimes(0)
  })
})

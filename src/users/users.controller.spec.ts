import { Test, TestingModule } from '@nestjs/testing'
import { UsersController } from './users.controller'
import { UsersService } from './users.service'
import { getRepositoryToken } from '@nestjs/typeorm'
import { User } from './user.entity'
import { MockRepository } from '../utils/testing/MockRepository'
import { ReviewsService } from '../reviews/reviews.service'

describe('UsersController', () => {
  let controller: UsersController

  const mockUsersService = {}
  const mockReviewsService = {
    findByUserUserName: jest.fn((userName) => ({ user: { userName } })),
  }

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [
        UsersService,
        { provide: getRepositoryToken(User), useClass: MockRepository },
        ReviewsService,
      ],
    })
      .overrideProvider(UsersService)
      .useValue(mockUsersService)
      .overrideProvider(ReviewsService)
      .useValue(mockReviewsService)
      .compile()

    controller = module.get<UsersController>(UsersController)
  })

  it('should be defined', () => {
    expect(controller).toBeDefined()
  })

  it('should return reviews for a particular user', () => {
    const userName = 'John Doe'
    expect(controller.findReviewsByUserName(userName)).toStrictEqual({
      user: { userName },
    })

    expect(mockReviewsService.findByUserUserName).toBeCalledTimes(1)
  })
})

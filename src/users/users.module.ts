import { Module } from '@nestjs/common'
import { UsersService } from './users.service'
import { UsersController } from './users.controller'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from './user.entity'
import { ReviewsService } from '../reviews/reviews.service'
import { Review } from '../reviews/review.entity'

@Module({
  imports: [TypeOrmModule.forFeature([User, Review])],
  controllers: [UsersController],
  providers: [UsersService, ReviewsService],
  exports: [UsersService],
})
export class UsersModule {}

import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Repository } from 'typeorm'

import { User } from './user.entity'
import { CreateUserDto } from './dto/create-user.dto'

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
  ) {}

  async findByUserName(userName: string) {
    return await this.usersRepository.findOne({ where: { userName } })
  }

  async create(user: CreateUserDto) {
    return await this.usersRepository.save(user)
  }

  async findOrCreate(userDto: CreateUserDto) {
    const user = await this.findByUserName(userDto.userName)
    if (!user) return await this.create(userDto)

    return user
  }
}

# This project is a NodeJs Challenge

I deployed a Swagger to play with the API endpoints [here](https://movie-review.tarasconi.com.ar/docs)

## Requirements
* Docker (with docker-compose-plugin)

## Installation

```bash
$ cp .env.example .env
$ # update environment variables in .env (only env that is required to be updated is THE_MOVIE_DATABASE_API_TOKEN)

$ docker compose --file docker-compose.production.yaml up --build --detach

```

## Running the app development

```bash
$ cp .env.example .env
$ # update environment variables in .env
$ docker-compose up -d
```

## Examples

Endpoints can be seen in /docs or SWAGGER_PATH env variable if set

## Stay in touch

- Author - [Gastón Tarasconi](https://www.linkedin.com/in/gastontarasconi/)

## License

Movie Review Challenge uses Nest and is [MIT licensed](LICENSE).


# Challenge
## NodeJs Challenge

### Overview:
Welcome to the Movie Review Challenge! In this challenge, you'll be building a movie review system using Node.js and
PostgresDB. The goal is to create an API that allows users to submit movie reviews and fetch movie information from
The Movie Database API. User management is not required for this project; a user's name will suffice for review
submissions.

### Detailed Tasks:
1. Review Submission Endpoint: Implement an endpoint that allows users to submit movie reviews. The review
should include the following fields:

    * TMDB ID - the unique identifier (ID) for each movie in TheMovieDb API
    * Reviewer's name
    * Rating - number on a scale of 1 to 10

Example request body for this endpoint:

```
POST /reviews
{
"tmdbId": 100
"userName": "John Doe",
"rating": 8
}
```

2. Database Setup:
Set up a PostgresDB to store movie reviews and movie information. Design a relational
structure that connects movies and their corresponding reviews. Ensure that the database prevents duplication of
movie data.

3. Integration with The Movie Database API:
 If a movie's data does not exist in the local database:

    * Fetch the following movie data from The Movie Database API (https://developer.themoviedb.org/docs
(https://developer.themoviedb.org/docs)): title, release date, poster, and overview.
    * Save the fetched data into the local database to avoid future external API calls for the same movie.

4. Review Retrieval Endpoints: Create two additional endpoints that:

    * Fetch all reviews of a particular movie along with its information (Title, release date, poster, and overview).
``` GET /movies/{tmdbId}/reviews ```

    * Fetch all reviews submitted by a specific user.
``` GET /users/{userName}/reviews ```

### Deliverables:

Your final submission should be a Git repository that includes:

* The source code of your solution using Node.js.
* A README file with instructions for setting up and running the project.
* Examples demonstrating the usage of all the implemented endpoints.

#### Additional Notes:

* Please approach this challenge as if it were a real-world project.
* You can choose to implement this challenge using either the Express.js or Nest.js framework. Both are valid
choices for this project, and you can decide based on your familiarity and comfort with the framework.
* Feel free to use any additional libraries or tools that you find helpful for this project.
* The focus of this challenge is on the functionality and implementation of the API, database, and integration with
* The Movie Database API. Creativity and code quality will be valued.

Good luck, and we look forward to reviewing your solution! If you have any questions or need clarifications, don't hesitate
to ask. Happy coding!

---
